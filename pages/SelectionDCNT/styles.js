import styled from "styled-components/native";
import { RectButton } from 'react-native-gesture-handler';

export const Container = styled.View`
    flex: 1;
    background-color: #f5f5f5;
    align-items: center;
    justify-content: center;
`;

export const Header = styled.View`
    background-color:  #f5f5f5;
    height: 60px;
    align-items: center;
    border-bottom-width: 0.5px;
    border-color: #43A72E;
    padding: 10px 20px;
    margin-top: 100px
`;

export const TextTitle = styled.Text`
    font-size: 24px;
    font-weight: bold;
    color: #494949;
    width: 100%;

`;

export const InputContainer = styled.View`
flex: 1;
width: 90%;
padding: 20px;
align-items: stretch;
background-color: #f5f5f5;
border-top-left-radius: 10px;
border-top-right-radius: 10px;
`;
export const ContainerInput = styled.View`
flex-direction: row;
`;
export const ForgotLogin = styled.View`
   margin: 20px 10px 0 10px;

`;

export const ForgotLoginText = styled.Text` 
  font-size: 15px;
  color: #43A72E;
  font-weight: bold;

`;

export const Button = styled(RectButton)`
    width: 274px;
    height: 55px;
    align-self: center;
    align-items: center;
    justify-content: center;
    background-color: #43A72E;
    border-radius: 50px;
    margin-top: 20px;
`;
export const ButtonSelected = styled(RectButton)`
    width: 300px;
    height: 55px;
    align-self: center;
    align-items: center;
    justify-content: center;
    background-color: #43A72E;
    border-radius: 20px;
    margin-top: 20px;
`;
export const ButtonContainer = styled.View``;

export const ButtonText = styled.Text`
  font-size: 19px;
  color: #FFF;
  font-weight: bold;
`;
