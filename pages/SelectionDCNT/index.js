import React, { useState } from 'react';
import { View, Text, Input, ScrollView, KeyboardAvoidingView, Alert } from 'react-native';
import { useNavigation } from "@react-navigation/native";
import { Picker } from "@react-native-picker/picker";

import Icon from 'react-native-vector-icons/EvilIcons';

//estilização da página
import {
  Container,
  Header,
  InputContainer,
  TextTitle,
  Button,
  ButtonSelected,
  ButtonContainer,
  ContainerInput,
  ForgotLogin,
  ForgotLoginText,
  ButtonText
} from "./styles";

export default function SelectionDCNT() {
  const nav = useNavigation();
  const [nameProduct, setNameProduct] = useState('');
  const dcnt = useState([
    {
      id: 1,
      name: 'Obesidade'
    },
    {
      id: 2,
      name: 'Diabetes'
    },
    {
      id: 3,
      name: 'Hipertensão'
    },
    {
      id: 4,
      name: 'Intolerância a Lactose'
    },
  ]);

  function handleLogin() {
    nav.navigate('Home');
  }

  function handleRecuperaLogin() {
    Alert.alert('Página de cadastro!');
  }

  return (
    <KeyboardAvoidingView
      style={{ flex: 1, backgroundColor: "#fff" }}
      behavior={Platform.OS == 'ios' ? 'padding' : undefined}
      enabled
    >

      <Container>
        <Header>
          <TextTitle>Dieto</TextTitle>
        </Header>
        <ScrollView >
          <View style={{ flexDirection: 'column', width: '100%' }}>
            <View style={{ flexDirection: 'row' }}>
              <ButtonSelected >
                <ButtonContainer>
                  <ButtonText>Diabetes</ButtonText>
                </ButtonContainer>
              </ButtonSelected>
            </View>
            <ButtonSelected >
              <ButtonContainer>
                <ButtonText>Hipertensão</ButtonText>
              </ButtonContainer>
            </ButtonSelected>
            <ButtonSelected >
              <ButtonContainer>
                <ButtonText>Obesidade</ButtonText>
              </ButtonContainer>
            </ButtonSelected>
            <ButtonSelected >
              <ButtonContainer>
                <ButtonText>Intolerância a Lactose</ButtonText>
              </ButtonContainer>
            </ButtonSelected>

          </View>
          <Button onPress={()=>handleLogin()}>
            <ButtonContainer>
              <ButtonText>Avançar</ButtonText>
            </ButtonContainer>
          </Button>
        </ScrollView>

      </Container>

    </KeyboardAvoidingView>
  );
}
